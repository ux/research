The goal of this research is to gather feedback and pain points from users about the [Snowflake landing page](https://snowflake.torproject.org/). During trainings, some participants demonstrated confusion on understanding the differences between the Snowflake Client and Proxy, resulting on installing the different version of the software that they wanted. 

After some modifications on the Snowflake landing page https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40209, we are going to research users' perceptions of the content of the page, with the support of Tor trainers.

# Introduction

Hi, how are you? I really appreciate you taking time out of your day to participate in this research.
So, let me outline how this will go. I’d like to start by asking you fill a quick survey (_hand the demographics survey to the participant_) and tell me a bit about your background. I will then ask you to perform some tasks on the Snowflake page. Once the tasks have been completed, I’d like to get some feedback from you about your experience with it.

The goal of this usability test to see how users interact with the Snowflake landing page, and to hear your thoughts on it. Sharing your honest thoughts are really important to better understand what can be improved in the page.

Is there anything you’d like to ask before we get going?

**Background questions**

Please, fill out this [demographic survey](https://gitlab.torproject.org/tpo/ux/research/-/issues/108#note_2878579).

**So, could you tell us about your work in the defense of human rights? And why do you need privacy and security to perform online activities?**

(Take notes of what the participant says)

## Tasks

Thank you for your answers. We’re now ready to start the test. Before we begin, I’d like to remind you of a few things.

First off, remember that **we aren’t testing you, we’re testing the page**. So if something isn’t working, or if you don't understand something, don’t worry. Please, think aloud as much as you can. I really want to hear your thoughts about the page we're testing, like where you’re navigating on the page, why you’re clicking somewhere, what you expect to happen when you do click, that sort of thing.
 
If you have questions, feel free to ask me, and I’ll answer all of them I can.

Finally, we’d like you to be as honest as possible. If something doesn’t make sense on the page, or it’s not working right, then feel free to tell us. You’re not going to hurt anyone's feelings, so don’t worry about that.

So, let’s begin. 



### Warm-up

Facilitator: Opens the computer in the Snowflake landing page and show it to the participant.

##### 1. Please take a look at the Snowflake page, take as much time as you need.

When the participant says they are ready, ask them: Please tell me what you saw and understood about this page.

Note to the researcher: If needed, tell again the participant to talk aloud their thoughts about the page.

### Snowflake content understanding.

##### 2. Please tell me how you would set up Snowflake on your computer to bypass censorship.

Note to the researcher: add any additional questions and explaining that might be necessary for the participant. Take notes.

##### 3. What about in your phone?


Note to the researcher: If the user installed Tor Browser in their mobile, ask them: Show me how you would use Snowflake on your phone.

Note to the researcher: Take notes of every step the user makes when trying to connect to Snowflake.



### User Support

##### 4. How would you look for help if you don't know how to connect to Snowflake?

Note to the researcher: add any additional questions and explaining that might be necessary for the participant. Take notes.


### Client x Proxy

##### 5. How can you use Snowflake to support other people in restricted regions circumvent censorship?

Note to the researcher: take notes of anything that the user says. Also note if the person is confused or not, if they have questions, anything that they say.

### Validate content

**Attention** note to the researcher: **you only need to perform questions 6-10 if you feel like the participant did not read the Snowflake landing page thoroughly, otherwise, jump to question 11.**

Note to the researcher: Open the Snowflake landing page again and show it to the user.

##### 6. Now let's go back a little, I want you to take your time and read all the content at the Snowflake page, take as much time as you need and then tell me what you're seeing.

Note to the researcher: take notes of the answer as detailed as possible.

##### 7. How would you use Snowflake to bypass censorship in your computer?

Note to the researcher: take notes of the answer as detailed as possible.

##### 8. And in your phone?

Note to the researcher: take notes of the answer as detailed as possible.

### User Support

##### 9. How would you look for help if you don't know how to connect to Snowflake?

Note to the researcher: take notes of the answer as detailed as possible.

### Client x Proxy

##### 10. How can you use Snowflake to support other people in restricted regions circumvent censorship?

Note to the researcher: take notes of the answer as detailed as possible.

### Wrap-up

##### 11. Before we finish, I’d like to ask you a few quick questions. Firstly, what did you think of the Snowflake page?

Note to the researcher: take notes of the answer as detailed as possible.


##### 12. Is there anything you’d like to add before we finish up?

Note to the researcher: take notes of the answer as detailed as possible.

**Thanks the participant.**
