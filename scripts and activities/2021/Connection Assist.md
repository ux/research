# User Research Plan

This is a moderated, in-person and assessment research plan. It means this study will happen with a research facilitator instructing participants, and the assessment research is used to test users' satisfaction and how well they are able to use the product proposed, as well its functionalities.

## Goals

- Evaluate whether users can easily identify where they need to navigate to connect to Tor in a censored context
- Measure how long users take to complete the given task to connect
- Find blockage points to use Connect Assist


## Audience

Human Rights Defenders, journalists, media activists, Tor training participants.

### Recruitment

We will ask Tor training participants to contribute with Tor by participating in this study. Participants of this research should join the testing just after the training, with understanding about how Tor works, and how to connect to bridges when using Tor Browser.

## Previous Research

https://gitlab.torproject.org/tpo/ux/research/-/issues/41
https://gitlab.torproject.org/tpo/ux/research/-/issues/16
https://gitlab.torproject.org/tpo/ux/research/-/issues/4

## Methodology

This is a moderated, in-person and assessment research plan.

## Activities

- [Getting to know the participant](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2021/get-to-know-users.md)
- Usability Testing - Connect Assist

### Description

This is a moderated, in-person and assessment research plan. It means this study happens with a research facilitator instructing participants, assessment research is used to test users' satisfaction and how well they are able to use the product presented, as well its functionalities.

### Suggested Time

10-15 minutes

### Materials Needed

- [Getting to know users script](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2021/get-to-know-users.md) printed, and pen to take notes
- Researcher facilitator needs to come with a computer with internet connection. Participants on this research need to use the facilitator computer in order to test the prototype.
- Paper and pen to take notes. 

### Outline and timing

#### 1. Introduction to the session

An introduction to the session will be made to all participants in the Tor training, so they understand what's the testing, and volunteer to do it.

Try scenarios: **[moderate](https://www.figma.com/proto/Vsh1aPOZGneDX4Zp27mjsK/Sponsor-30?page-id=531%3A2047&node-id=915%3A5746&viewport=241%2C48%2C0.1&scaling=min-zoom&starting-point-node-id=915%3A5746&show-proto-sidebar=1)** and **[severe](https://www.figma.com/proto/Vsh1aPOZGneDX4Zp27mjsK/Sponsor-30?page-id=531%3A2047&node-id=915%3A7383&viewport=241%2C48%2C0.1&scaling=min-zoom&starting-point-node-id=915%3A7383&show-proto-sidebar=1) censorship**.

#### 2. Activity

##### Task 1
Describe your general thoughts about this page (**Connect to Tor**). What information are you seeing? What can you do on this page? 

##### Task 2
How would you connect to your project website?

##### Task 3
What do you see? What do you think just happened? How would you try to connect?

_Participant can try "Configure connection" or "Try a Bridge"._

_Notes for the facilitator: how many participants clicked on "Configure connection" or "Try a Bridge"?_

##### Task 4 - Option "Configure connection"
What do you see? How would you configure the connection?

_Notes for the facilitator: did the participant successfully connected to Tor? If not, where did they stuck? Where did they click?_

##### Task 4 - Option "Try a Bridge"
What just happened? Are you satisfied with the results?

#### 3. Debriefing

Follow-up questions:

- How hard was it to connect to Tor in a censored environment?
- What did you expect to happen?
- Did you see that Tor was using your location to find a bridge? (if user clicked on 'Try a bridge')
- If you had three wishes to make your experience better when navigating with the Tor Browser, what would you wish for?

## Report Findings


## Attachments

- https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2021/get-to-know-users.md
