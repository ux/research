# Open Research Questions 
**Usability, User Experience and User Interface**

## Network Level

- How to measure perceived performance without compromising users' privacy?
- How to improve exit relay IP reputation to reduce friction while browsing (e.g., Captchas)?

## Client Level - Browser

- How to measure websites breakage without compromising users' safety?
- How to improve user understanding between new tab, new circuit, and new identity? 
- How to improve user experience on exiting in random languages?
- What is the best way to show users their fingerprint status?

## Client Level - Tor Tunnel

- What is the best interface for a desktop tunneling app?
- What is the most effective way to explain users' trade-offs between client vs. network safety measures?
