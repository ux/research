# Entrevista - Guía de preguntas 

Hola que tal [PERSONA]. ¿Cómo va todo?  :). 

### Introducción:

- Ok. Primero que nada, gracias por el espacio para hacer esta entrevista. --- 
- Segundo, sé que lo he dicho antes, pero me parece importante mencionarlo. En teoría, no deberíamos durar más de 1h pero puede que duremos menos. Entonces para que lo sepa. 
- Tercero, preguntarle si ¿puedo grabar esta llamada para volverla escuchar y así tomar notas y demás?
- Cuarto, sólo pedirle completa honestidad en la conversación, decir algo negativo no es malo. Más bien queremos aclarar y delimitar cuáles podrían ser los problemas que podría tener Tor al acercarse a comunidades similares que necesitan este tipo de tecnología. 

### Recomendaciones:

1. ¿En qué casos sí recomendaría Tor y en qué casos no lo recomendaría? 

### Fricciones:

##### Jerarquía:

1. ¿Cuáles son las barreras más importantes que la gente enfrentaría al usar Tor Browser?
   1. ¿Causa? ¿tipo de uso? ¿técnico?
   2. ¿Estas fricciones son por el nivel de conocimiento técnico de la persona, o por el uso que quieren hacer (el cual es complicado)?

##### Técnicas:

1. ¿Cómo cree que se podría solucionar estos problemas?

##### Conceptuales:

1. ¿Cree que es importante que Tor deba explicar conceptos de privacidad a los que desea acercarse?
2. ¿Cómo debería abordar estos temas?

##### Hipotético:

Con estas posibles soluciones que hemos hablado, ¿hay algún cambio en los casos? Es decir ¿ahora sí le recomendaría a esas personas usarlo? ¿o hay algunas que todavía aún no le recomendaría?

1. ¿cuáles sí recomendaría?
2. ¿por qué aún no recomendaría?

### Cierre

¿Tiene alguna pregunta sobre Tor Browser que le gustaría conversar?

##### Despedida:

Bueno, perfecto. De nuevo, quiero agradecerte por participar en toda esta investigación y por el tiempo que me has brindado. 