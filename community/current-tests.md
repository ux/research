## Open User Research
In this page you find the current needs to be run when testing Tor products, as well as methodologies and testing scripts. Before running a Tor user research, be sure you read our [Guidelines to be a User Researcher with Tor](#).

### Tor Browser for Desktop
* [User Research: Onboarding](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2019/4._Tor_Browser_Desktop_-_download__launch__browse.pdf)
* [User Research: Security Settings](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2019/2.Tor_Browser_Desktop_-_Security_Settings.pdf)
* [User Research: New Identity](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2019/new_identity_-_en.pdf)
* [User Needs Discovery](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2019/1._User_Needs_Discovery_-_Tor_Browser_Desktop.pdf)

### Tor Browser for Android
* [User Research: Onboarding](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2019/3._Tor_Browser_Android_-_download__launch__browse.pdf)
* User Research: Security Settings (in development)
* User Research: New Identity (in development)
* [User Needs Discovery](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2019/1._User_Needs_Discovery_-_Tor_Browser_Desktop.pdf)

### Onion Services
* User Research: Onion Security Indicator (in development)

## Past User Research
On our commitment with an open design, we make public here our user research ran through the global-south. If you want to run user research with us, please [get in touch.](https://lists.torproject.org/cgi-bin/mailman/listinfo/ux)

| Project  | Methodology | Locations | Dates | Reporting |
| -------- | ----------- | --------- | --------- | ----- |
| Tor Launcher | Usability testing ([.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2018/1.India_User_testing_Tor_Launcher_Test.pdf)) | Mumbai(IN) | Q118 | [.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/reports/2018/india-2018-torlauncher-interview.pdf) |
| Onion Security Indicator | Usability testing ([.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2018/2.User_testing_.onion_states_Test.pdf)) | Mumbai(IN), Kampala(UG), Valencia(ES), Mombasa(KE)| Q118, Q218 | [.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/reports/2018/ur_kenya_2018_Feature_report__Onions_and_Circuit_Display.pdf) |
| TB Circuit Display | Usability testing ([.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2018/3.User_testing_circuit_display_Test.pdf)) | Kampala(UG), Nairobi(KE), Mombasa(KE) | Q118, Q218 | [.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/reports/2018/ur_kenya_2018_Feature_report__Onions_and_Circuit_Display.pdf) |
| Tor Browser for Desktop | User needs discovery ([.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2018/4.00_user_needs_discovery.pdf)) | Bogotá(CL), Cali(CL), Valle del Cauca(CL), Kampala(UG), Hoima(UG), Nairobi(KE)  | 2018 | [.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/reports/2018/ur_colombia_2018_report_tbb-tba.pdf) |
| Tor Browser for Android | User needs discovery ([.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2018/4.00_user_needs_discovery.pdf)) | Bogotá(CL), Cali(CL), Valle del Cauca(CL), Kampala(UG), Hoima(UG), Nairobi(KE)  | 2018 | [.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/reports/2018/ur_colombia_2018_report_tbb-tba.pdf) |
| Tor Users Demographics | Survey ([.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/scripts%20and%20activities/2018/6.Spanish_-_Tor_Project_Survey_-_Tor_Users_Demographics.pdf)) | Online  | 2018 | [.pdf](https://dip.torproject.org/torproject/ux/research/blob/master/reports/2018/user_demographics_2018.pdf) |
