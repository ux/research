# User Research Plan

This is a moderated and remote usability testing, and assessment. It means this study will happen with a research facilitator instructing participants, and the assessment research is used to test users' satisfaction and how well they are able to use the product proposed, as well its functionalities.

## Goals

- Evaluate whether users can easily identify where they need to navigate to connect to Tor in a censored context
- Measure how long users take to complete the given task to connect
- Find blockage points to use Connect Assist


## Audience

Human Rights Defenders, journalists, media activists, Tor training participants.

### Recruitment

We are looking for participants in countries where the internet can be potentially censored. The recruitment won't happen in public channels to protect users privacy and security, and we will get in touch with human rights defenders organizations and participants individually.

#### Linked issues

https://gitlab.torproject.org/tpo/ux/research/-/issues/83

### Previous Research

https://gitlab.torproject.org/tpo/ux/research/-/issues/41
https://gitlab.torproject.org/tpo/ux/research/-/issues/16
https://gitlab.torproject.org/tpo/ux/research/-/issues/4

## Methodology

This is a moderated and remote usability testing, and assessment.

## Activities

- [Before the testing](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2022/user-interview.md)
- Usability Testing - Connect Assist (this file)

### Description

This is a moderated and remote usability testing and assessment. It means this study happens with a research facilitator instructing participants, assessment research is used to test users' satisfaction and how well they are able to use the product presented, as well its functionalities.

### Suggested Time

10-15 minutes

### Materials Needed

- [Before the testing](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2022/user-interview.md)
- Use a Riseup Pad to take notes
- A room in a video conference platform
- Researcher facilitator and participants should use a computer (desktop/laptop) with good internet connection (in case it is need to screen share).

### Outline and timing

#### 1. Introduction to the session

An introduction to the session will be made to participants.

Users are instructed to go to [Tor Browser Alpha Download Page](https://www.torproject.org/download/alpha/):

1. Download Tor Browser Alpha
2. Install Tor Browser Alpha
3. Open Tor Browser Alpha

#### 2. Activity

##### Task 1
Describe your general thoughts about this page (**Connect to Tor**). What information are you seeing? What can you do on this page? 

##### Task 2
How would you connect to your project website?

##### Task 3
What do you see? What do you think just happened? How would you try to connect?

_Participant can try "Configure connection" or "Try a Bridge"._

_Notes for the facilitator: how many participants clicked on "Configure connection" or "Try a Bridge"?_

##### Task 4 - Option "Configure connection"
What do you see? How would you configure the connection?

_Notes for the facilitator: did the participant successfully connected to Tor? If not, where did they stuck? Where did they click?_

##### Task 4 - Option "Try a Bridge"
What just happened? Are you satisfied with the results?

#### 3. Debriefing

Follow-up questions:

- How hard was it to connect to Tor in a censored environment?
- What did you expect to happen?
- Did you see that Tor was using your location to find a bridge? (if user clicked on 'Try a bridge')
- If you had three wishes to make your experience better when navigating with the Tor Browser, what would you wish for?

## Report Findings

