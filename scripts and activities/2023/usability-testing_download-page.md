## Download Page Usability Testing

### Introduction

**Greetings**: Hi, how are you? I really appreciate you taking time out of your day to participate in this research.

So, let me outline how this will go. I’d like to start by asking you fill a quick survey and tell me a bit about your background. I will then ask you some questions about your online browsing, and then to perform some tasks at the Tor Browser Download Page. Once the tasks have been completed, I’d like to get some feedback from you about your experience with it.

The goal of this usability testing is to see how users interact with the Download page, if they succeed downloading Tor Browser and to hear your thoughts on it. Sharing your honest thoughts are really important in this testing.

Is there anything you’d like to ask before we get going?

**Starting**

First off, remember that we aren’t testing you, we’re testing Tor's download page. So if something isn’t working, or if you don't understand something , don’t worry. Please, think aloud as much as you can. I really want to hear your thoughts about the page we're testing, like where you’re navigating on the page, why you’re clicking somewhere, what you expect to happen when you do click, that sort of thing. If you have questions, feel free to ask me, and I’ll answer all of them I can.

Finally, we’d like you to be as honest as possible. If something doesn’t make sense on the page, or it’s not working right, then feel free to tell us. You’re not going to hurt anyone's feelings, so don’t worry about that.

So, let’s begin.

**First task**

Please, fill out this demographic survey. (After the participant completed the survey, ask them:)

##### So, can you tell me about your preferences when you're browsing the internet? What devices do you use to connect and what browsers do you use in each device?
(Take notes of what the participant says)


### Browser - User Needs

If the participant use more than one browser: 
##### Which of these browsers do you use the most? Why?
(Take notes of what the participant says)

##### Have you used Tor Browser before? If yes, in what device(s)?
(Take notes of what the participant says)


##### If yes, please tell me more about when do you use Tor Browser. 
(If you need to clarify, ask them if they use for work, research, or anything else)
(Take notes of what the participant says)


##### What are the 3 main characteristics do you look for in a browser? 
(If you need to clarify, ask if it is quick install, compatibility, multiple profiles, anonymous tab, etc)(Take notes of what the participant says)




### Download page

Note to the researcher: open the desktop computer and open the prototype link that was sent to you. When it's ready, tell the participant:

##### Imagine that you're going to download Tor Browser for Desktop. You enter google.com in your default browser, and these are the results. The Tor Project page is the first result and you click there.  

(ask the participant to click on the search results, it will then jump to the Tor Project Download Page).


##### Please tell me what is the first thing you want to do when you look at this page. Where do you want to click?
(Take notes of what the participant says)



##### Among these words, please select the 3 that resonates the most on how you felt about the colors and illustrations in this page. 
(circle the words chose by the participant)

accessible, appealing, attractive, boring, clean, collaborative, confusing, consistent, creative, disconnected, energetic, engaging, exciting, friendly, fun, frustrating, helpful, inspiring, intuitive, overwhelming, rigid, simplistic, sophisticated, too-technical, useful.

##### Please explain why did you choose these words.
(Take notes of the participant answer)


##### What are the advantages of using Tor Browser based on what you just saw at this page?
(Take notes of the participant answer)



##### Can you tell me how would you download Tor Browser on your computer based on this page?
(Take notes of the participant answer)



##### After downloading Tor Browser, what would you do?
(Take notes of the participant answer)



##### How much verifying signatures is important to you? Please, explain.
(Take notes of the participant answer)



##### If you want to support the Tor Project by testing Tor Browser Alpha or Nightly, how would you do it?
(Take notes of the participant answer)


### Wrap-up

Before we finish, I’d like to ask you a few quick questions.
##### Overall, what do you think about the download page?
(Take notes of the participant answer)

##### Is there anything you’d like to add before we finish up?
(Take notes of the participant answer)
