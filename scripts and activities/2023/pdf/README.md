**List of files and documents to print and use during User Research activities**.

How to create and upload files:
- Rename your file as name-of-document-language.pdf
- Avoid capitalized letters. Instead, use lowercase.
- Avoid spaces and special character.
