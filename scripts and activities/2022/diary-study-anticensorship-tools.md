
# User Research Plan

This is a main User Research Plan to be coordinated with stakeholders and to be done in target regions. 

## Goals

The goal of this plan is to test anticensorship features in Tor Browser for Desktop, Tor Browser for Android and Onionshare Mobile with users in the target regions.

## Audience

We are looking for low-risk users that are able to test the anticensorship tools listed in the region. Users need to have a computer and a cell phone, as well as an internet connection in order to complete the test. Users will engange with this research during a whole week.

### Recruitment

We will launch a Call for Participation in Tor Community channels, partners' contacts and any other Internet forum for digital rights, such as Team Community. 

The Call for Participation contain a small description of the research as well as some questions to screen participants that can participate in this study. After the Call for Participation, we will recruit five participants to engage. If we don't have enough participants volunteering, we will try another round of recruitment until we have enough participants for the research.

## Previous Research

- https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2022/connect-assist-independent.md

## Methodology

- Non-moderated research, diary study.
- Users will be onboarded by email and pre-recorded video, and can ask questions about this study using the same communication channeles.
- Users will be asked to perform daily tasks using the tools of this study and report to the User Researchers.
- After a complete week of participation, users will be compensated with a gift card provided by Tor.
- The report will be published online in Tor's Gitlab repository completely anonymized, and will be shared by email with participants.

## Activities

- Test [Connection Assist in Tor Browser for Desktop](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2022/connect-assist-independent.md).
- Test Connection Assist testing in Tor Browser for Android
- Test Onionshare mobile: to be shared by TGP

### Description

> A diary study is a research method used to collect qualitative data about user behaviors, activities, and experiences over time. In a diary study, data is self-reported by participants longitudinally — that is, over an extended period of time that can range from a few days to even a month or longer. During the defined reporting period, study participants are asked to keep a diary and log specific information about activities being studied. To help participants remember to fill in their diary, sometimes they are periodically prompted (for example, through a notification received daily or at select times during the day). By [NNGroup](https://www.nngroup.com/articles/diary-studies/)

### Suggested Time

Around a hour per day, during a workday week.

### Materials Needed

Participants need:

- computer desktop or laptop
- mobile phone
- internet connection

### Outline and timing

#### 1. Introduction to the session

- Users will be onboarded by e-mail and online videos. 
- Users can engage in one or more tool. 
- They will be compensated for each tool they test. 

#### 2. Activity

- [Connection Assist testing in Tor Browser for Desktop](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2022/connect-assist-independent.md)
- Connection Assist testing in Tor Browser for Android
- Onionshare mobile: to be shared by TGP

#### 3. Issues

- Test Tor Browser for Desktop: https://gitlab.torproject.org/tpo/ux/research/-/issues/83
- Test Tor Browser for Android: https://gitlab.torproject.org/tpo/ux/research/-/issues/102
- Test OnionShare: https://gitlab.torproject.org/tpo/ux/research/-/issues/101

## Report Findings

Report will be shared by the end of this research. 

## Attachments

